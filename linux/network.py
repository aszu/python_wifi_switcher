"""
This file contains all things that are related to linux's network
interface operation - bringing it down/up and changing MAC address.

Probably also reading some values. In future. :)
"""
import subprocess
import pathlib

bringInterfaceSomewhere = lambda where: lambda interface: (
    subprocess.call(['ip', 'link', 'set', 'dev', interface, where])
)

bringInterfaceDown = bringInterfaceSomewhere('down')

bringInterfaceUp = bringInterfaceSomewhere('up')

def setInterfaceMACAddress(interface, mac):
    subprocess.call(['ip', 'link', 'set', interface, 'address', mac])

isInterfaceWireless = lambda interface: (
    (interface / 'wireless').exists()
)

def getNetworkInterfaces(wirelessOnly = False):
    p = pathlib.Path('/sys/class/net')
    pathCandidates = list(p.iterdir())
    if wirelessOnly:
        interfacePaths = filter(isInterfaceWireless, pathCandidates)
    else:
        interfacePaths = pathCandidates
    return list(map(lambda x: x.stem, interfacePaths))

def getDevicesForInterfaces(interfaces):
    return list(
        map(
            lambda interface: str((pathlib.Path('/sys/class/net/'+interface) / 'device').resolve()),
            interfaces
        )
    )