def indexByRevSubstr(item, lookupList):
    print("indexBy")
    i = 0
    while i < len(lookupList) - 1:
        if item.find(lookupList[i]) != -1:
            return i
        i += 1
    return len(lookupList)


def getPreference(item, preferenceList):
    return indexByRevSubstr(item, preferenceList)

preferencePredicate = lambda prefList: lambda item: getPreference(item, prefList)

def sortByPreference(itemList, preferenceList):
    return sorted(itemList, key = preferencePredicate(preferenceList))